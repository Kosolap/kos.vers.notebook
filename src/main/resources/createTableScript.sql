USE test;

DROP TABLE IF EXISTS image, comment, document, theme;

CREATE TABLE theme (
  theme_id INT NOT NULL AUTO_INCREMENT,
  theme_name VARCHAR(200),
  user_id INT,
  PRIMARY KEY(theme_id)
) ENGINE=InnoDB CHARACTER SET utf8;

CREATE TABLE user (
  user_id INT NOT NULL AUTO_INCREMENT,
  user_login VARCHAR(200),
  user_email VARCHAR(200),
  user_password VARCHAR(200),
  user_name VARCHAR(200),
  enabled BIT,
  PRIMARY KEY(user_id)
) ENGINE=InnoDB CHARACTER SET utf8;

CREATE TABLE user_roles (
  user_role_id INT NOT NULL AUTO_INCREMENT,
  role VARCHAR(200),
  username VARCHAR(200),
  PRIMARY KEY(user_role_id)
) ENGINE=InnoDB CHARACTER SET utf8;

CREATE TABLE document (
  document_id INT NOT NULL AUTO_INCREMENT,
  document_name VARCHAR(200),
  document_value TEXT,
  document_mindmap TEXT,
  theme_id INT NOT NULL,
  PRIMARY KEY(document_id),
  FOREIGN KEY(theme_id)
  REFERENCES theme (theme_id)
) ENGINE=InnoDB CHARACTER SET utf8;

CREATE TABLE comment (
  comment_id INT NOT NULL AUTO_INCREMENT,
  document_id INT NOT NULL,
  comment_name VARCHAR(50),
  comment_text TEXT,
  PRIMARY KEY(comment_id),
  FOREIGN KEY(document_id)
  REFERENCES document (document_id)
) ENGINE=InnoDB CHARACTER SET utf8;

CREATE TABLE image (
  image_id INT NOT NULL AUTO_INCREMENT,
  document_id INT NOT NULL,
  image_name VARCHAR(100),
  image_first_url TEXT,
  image_second_url VARCHAR(500),
  image_third_url VARCHAR(500),
  bs_code TEXT,
  PRIMARY KEY(image_id),
  FOREIGN KEY(document_id)
  REFERENCES document (document_id)
) ENGINE=InnoDB CHARACTER SET utf8;
