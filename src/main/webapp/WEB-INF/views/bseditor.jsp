<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="slayfi.jrnotebook.model.*" %>

<html>
<head>
    <title>Просмотр документа</title>
    <link rel="stylesheet" type="text/css" href="/css/canvasStile.css">
</head>
<body>

    <div class="CanvasContainer">
        <canvas id="drawingCanvas" width="800" height="500"></canvas>
    </div>

    <table>
        <td>
            <div class="Toolbar">
            -Operations-<br>
            <button onclick="saveCanvas()">Save Canvas</button>
            <button onclick="loadCanvas()">Load Canvas</button>
            <button onclick="clearCanvas()">Clear Canvas</button>
            <button onclick="setText()">Put text</button>
            <button onclick="deletObj()">Delete object</button>
            </div>

            <div class="Toolbar" >
            - Figures -<br>
            <img src="/icons/rectangle.gif" alt="Прямоугольник" onclick="addRectangle()">
            <img src="/icons/brill.gif" alt="Ромб" onclick="addBrill()">
            <img src="/icons/ellipse.gif" alt="Овал" onclick="addEllipse()">
            <img src="/icons/tCicl.gif" alt="Верхняя граница цикла" onclick="addTopCicle()">
            <img src="/icons/dCicl.gif" alt="Нижняя граница цикла" onclick="addDownCicle()">
            </div>
        </td>
    </table>



    <form action="/upload_by_url" method="post" id="saveForm">

        <input name="url" type="hidden" id="url-field">
        <br>
        <input name="imageName" type="hidden" id="image_name-field">
        <br>
        <input name="document_id" type="hidden" id="url_doc_id">
        <br>
        <input name="type" type="hidden" value="blockScheme">
        <br>
        <input name="bscode" type="hidden" id="bscode_value">


    </form>

    <script type="text/javascript" src="/js/canvas.js">
    </script>

    <script>
        <%
        String str = request.getParameter("id");
        str = str.replaceAll("N",".");


        if(str.length() > 1)
         str = Mapper.getImageBsCode(str);

        %>

        var test = "<%=str%>";

        var args = top.tinymce.activeEditor.windowManager.getParams();

        loadFromString(test);

    </script>

</body>
</html>