<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kosolap
  Date: 18.05.2015
  Time: 5:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

    <h1>ЭТО ТЕСТОВАЯ СТРАНИЦА ДЛЯ РАБОТЫ С ПОЛЬЗОВАТЕЛЯМИ</h1>

    <br>

    <table>

        <tr>
            <td>
                №
            </td>

            <td>
                Login
            </td>

            <td>
                Password
            </td>

            <td>
                Name
            </td>

            <td>
                Email
            </td>
        </tr>

        <c:forEach var="user" items="${allUsers}">
        <tr>
            <td>
                ${user.userId}
            </td>

            <td>
            <a href="<c:url value='/userinfo/${user.userId}'/>">${user.userLogin}</a>
            </td>

            <td>
                ${user.userPassword}
            </td>

            <td>
                ${user.userName}
            </td>

            <td>
                ${user.userEmail}
            </td>

            <td>
                <form  action="/deleteuser" method="post">
                    <input type="hidden" value="${user.userId}" name="id">
                    <input type="submit" value="Удалить">
                </form>
            </td>
        </tr>

        </c:forEach>

    </table>

    <br>

    <form  action="/adduserform">
        <input type="submit" value="Добавить пользователя">
    </form>

    <form  action="/index">
        <input type="submit" value="Назад">
    </form>

</body>
</html>
