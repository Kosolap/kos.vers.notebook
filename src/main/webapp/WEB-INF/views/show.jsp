<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Просмотр документа</title>
    <link rel="stylesheet" href="/css/mindmap.css">
    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
    //<![CDATA[
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
    //]]>
    </script>
</head>
<body>
    <h1 align="center">${document.name}</h1>
    <p>${document.mindmap}</p>
    <p>${document.value}</p>
    <br>
    <hr>
    <c:forEach var="comment" items="${document.comments}">
        <b>${comment.name}:</b>
        <br>
        "${comment.text}"
        <hr>
    </c:forEach>

    <form:form method="post" action="/comment/${document.documentId}" modelAttribute="comment">
        <label for="name-field">Ваше имя:</label>
        <form:input path="name" id="name-field"/><br>
        <label for="comment-field">Ваш комментарий:</label>
        <form:textarea path="text" id="comment-field" cols="50" rows="8"/><br>
        <input type="submit" value="Отправить">
    </form:form>

  <a href="<c:url value='/index'/>">Вернуться назад</a>
</body>
</html>
