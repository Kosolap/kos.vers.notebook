<%@ page import="slayfi.jrnotebook.model.Document" %>
<%@ page import="java.util.List" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  Created by IntelliJ IDEA.
  User: Kosolap
  Date: 21.05.2015
  Time: 1:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>ТУТ БУДЕТ ИНФОРМАЦИЯ О ПОЛЬЗОВАТЕЛЕ</h1>
    <table>
        <tr>
        <td>
            ${user.userId}
        </td>
        </tr>
        <tr>
            <td>
                ${user.userLogin}
            </td>
        </tr>
        <tr>
            <td>
                ${user.userPassword}
            </td>
        </tr>
        <tr>
            <td>
                ${user.userName}
            </td>
        </tr>
        <tr>
            <td>
                ${user.userEmail}
            </td>
        </tr>
    </table>


<c:choose>
    <c:when test="${notEmpty =='true'}">
        <table>
        <tr>
            <th>id</th>
            <th>Имя документа</th>
            <th style="width: 400px;">"Показать"</th>
            <th>"Редактировать"</th>
            <th>"Удалить"</th>
        </tr>
        <c:forEach var="document" items="${allDocuments}">
            <tr>
                <td>${document.documentId}</td>
                <td><b>${document.name}</b></td>
                <td><a href="<c:url value='/show/${document.documentId}'/>">Показать</a></td>
                <td><a href="<c:url value='/edit/${document.documentId}'/>">Редактировать</a></td>
                <td><a href="<c:url value='/delete/${document.documentId}'/>">Удалить</a></td>
            </tr>
        </c:forEach>
        </table>
    </c:when>

    <c:otherwise>
    </c:otherwise>
</c:choose>

<form  action="/admin/users">
    <input type="submit" value="Назад">
</form>

</body>
</html>
