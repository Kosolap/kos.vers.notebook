<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <title>Mindmap</title>
  <link rel="stylesheet" href="/css/mindmap.css">
</head>
<body>
  <script language="javascript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="/js/mindmap.js"></script>
  <script type="text/javascript">
    $(function(){
      $('.mindmap').mindmap();
    });
  </script>

<div class="mindmap">
  <ol class="children children_leftbranch">
    <li class="children__item">
      <div class="node">
        <div class="node__text"><img src="http://caen.pro/wp-content/uploads/2013/02/cute_ball_go21-300x300.png" width="20" height="20"></div>
        <input type="text" class="node__input">
      </div>
      <ol class="children">
        <li class="children__item">
          <div class="node">
            <div class="node__text">Node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">Node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">Node</div>
            <input type="text" class="node__input">
          </div>
        </li>
      </ol>
    </li>
    <li class="children__item">
      <div class="node">
        <div class="node__text">node</div>
        <input type="text" class="node__input">
      </div>
      <ol class="children">
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text"><img src="http://caen.pro/wp-content/uploads/2013/02/cute_ball_go21-300x300.png" width="20" height="20"></div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
      </ol>
    </li>
    <li class="children__item">
      <div class="node">
        <div class="node__text">node</div>
        <input type="text" class="node__input">
      </div>
      <ol class="children">
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text"><img src="http://caen.pro/wp-content/uploads/2013/02/cute_ball_go21-300x300.png" width="20" height="20"></div>
            <input type="text" class="node__input">
          </div>
        </li>
      </ol>
    </li>
  </ol>
  <div class="node node_root">
    <div class="node__text">node</div>
    <input type="text" class="node__input">
  </div>
  <ol class="children children_rightbranch">
    <li class="children__item">
      <div class="node">
        <div class="node__text">node</div>
        <input type="text" class="node__input">
      </div>
      <ol class="children">
        <li class="children__item">
          <div class="node">
            <div class="node__text"><img src="http://caen.pro/wp-content/uploads/2013/02/cute_ball_go21-300x300.png" width="20" height="20"></div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
      </ol>
    </li>
    <li class="children__item">
      <div class="node">
        <div class="node__text">node</div>
        <input type="text" class="node__input">
      </div>
      <ol class="children">
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
      </ol>
    </li>
    <li class="children__item">
      <div class="node">
        <div class="node__text">node</div>
        <input type="text" class="node__input">
      </div>
      <ol class="children">
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
        <li class="children__item">
          <div class="node">
            <div class="node__text">node</div>
            <input type="text" class="node__input">
          </div>
        </li>
      </ol>
    </li>
  </ol>
</div>


</body>
</html>