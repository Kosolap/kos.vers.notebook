<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
      <title>Добавить/Редактировать</title>
    <script type="text/javascript" src="/libs/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
      tinymce.init({
        selector: "textarea",
        theme: "modern",
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
        font_size_style_values: "12px,13px,14px,16px,18px,20px",
        plugins: [
          "jrimage textcolor table preview lists link jrbseditor"
    ],
    menubar : false,
    toolbar: "undo redo | fontselect | fontsizeselect | forecolor backcolor | bold italic underline strikethrough superscript subscript | jrimage | jrbseditor | link unlink | alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist | table | preview | removeformat"
    });
    </script>
  </head>
  <body>
    <h3 align="center">Добавление/Редактирование статьи</h3>
      <form:form method="post" action="/edit" modelAttribute="document" onsubmit="convertImagesToMarkers()">
      <h4 align="center">
        <label for="theme-field">Тема:</label>
        <form:input path="name" style="width: 250px;" id="theme-field"/>
      </h4>
        <form:textarea id="editor1" rows="32" path="value" style="width: 100%;"/>
        <form:hidden path="theme"/>
        <form:hidden id="doc_id" path="documentId"/>
        <input type="hidden" name="urls_and_names" id="urls_and_names_field">
        <input type="submit" value="Сохранить"/>
      </form:form>
  <script>
      function convertImagesToMarkers() {
          var images = new Array();
          <c:forEach items="${images}" var="item">
            images.push("${item}");
          </c:forEach>

          //alert('Java массив ' + images);
          var newImages = document.getElementById('urls_and_names_field').value.split(' ');
          newImages.splice(newImages.length - 1, 1);
          //alert('новый' + newImages);
          images = images.concat(newImages);
          //alert('общий массив ' + images);

          var plainText = tinyMCE.activeEditor.getContent({format : 'html'});
          //alert('Весь текст редактора: ' + plainText);
          var imgRegexp = /<img .*\/>/g;
          var imagesInEditor = plainText.match(imgRegexp);

          if(imagesInEditor) {
              for(var i = 0; i < imagesInEditor.length; i++) {
                  if((imagesInEditor[i].split('alt').length - 1) > 1) {
                      //alert('элемент с множеством тегов ' + imagesInEditor[i]);
                      var positionStart = 0;
                      var positionEnd = 0;
                      var start = imagesInEditor[i].indexOf('<img', positionStart);
                      var end = imagesInEditor[i].indexOf(' \/>', positionEnd) + 3;
                      while(start != -1) {
                          positionStart = start + 1;
                          end = imagesInEditor[i].indexOf(' \/>', positionEnd) + 3;
                          positionEnd = end + 1;
                          var image = imagesInEditor[i].substring(start, end);
                          //alert('вырезанный тег' + image);
                          imagesInEditor.push(image);
                          start = imagesInEditor[i].indexOf('<img', positionStart);
                      }
                      imagesInEditor.splice(i, 1);
                  }
              }
          }

          //alert('Все найденные изображения: ' + imagesInEditor);
          //alert('Количество циклов ' + imagesInEditor.length);
          if(imagesInEditor) {
              for (var j = 0; j < imagesInEditor.length; j++) {
                  var urlStart1 = imagesInEditor[j].indexOf('/images/');
                  var urlStart2 = imagesInEditor[j].indexOf('http');
                  var urlStart3 = imagesInEditor[j].indexOf('data:');
                  var endOfStandartMarker1 = imagesInEditor[j].indexOf('" alt=') - 4;
                  var endOfStandartMarker2 = imagesInEditor[j].indexOf('" alt=');
                  var startWidth = imagesInEditor[j].indexOf('width="');
                  var endWidth = imagesInEditor[j].indexOf('" height="');
                  var startHeight = endWidth + 10;
                  var endHeight = imagesInEditor[j].indexOf('" \/');
                  var startLeftRightAlign = imagesInEditor[j].indexOf('float: ');
                  var endLeftRightAlign = imagesInEditor[j].indexOf(';" ');
                  var startCenterAlign = imagesInEditor[j].indexOf('display: block; margin-left: auto; margin-right: auto;');
                  var startId = imagesInEditor[j].indexOf('id="');
                  var endId = imagesInEditor[j].indexOf('"', startId + 4);

                  //alert('Берем элемент: ' + imagesInEditor[j]);
                  var markerWithParam = '$|||N:';
                  //alert('Начало маркера ' + markerWithParam);
                  if (urlStart2 != -1) {
                      markerWithParam += imagesInEditor[j].substring(urlStart2, endOfStandartMarker1) + ":N";
                  } else if (urlStart2 == -1 && urlStart1 != -1) {
                      markerWithParam += imagesInEditor[j].substring(urlStart1 + 8, endOfStandartMarker1) + ":N";
                  } else if (urlStart2 == -1 && urlStart3 != -1) {
                      markerWithParam += imagesInEditor[j].substring(urlStart3, endOfStandartMarker2) + ":N";
                  }

                  if (startWidth != -1) {
                      markerWithParam += 'W:' + imagesInEditor[j].substring(startWidth + 7, endWidth) + ':W' +
                              'H:' + imagesInEditor[j].substring(startHeight, endHeight) + ':H';
                     // alert('Маркер с параметрами ' + markerWithParam);
                  }
                  if(startLeftRightAlign != -1) {
                      markerWithParam += 'S:' + imagesInEditor[j].substring(startLeftRightAlign + 7, endLeftRightAlign) + ':S';
                    // alert('Маркер с позицией в тексте(справа, слева) ' + markerWithParam);
                  }
                  if(startCenterAlign != -1) {
                      markerWithParam += 'S:center:S';
                     // alert('Маркер с позицией в тексте(центр) ' + markerWithParam);
                  }
                  if(startId != -1) {
                      markerWithParam += 'ID:' + imagesInEditor[j].substring(startId + 4, endId) + ':ID';
                  }
                  markerWithParam += '|||$';
                  //alert('Итоговый маркер на замену ' + markerWithParam);
                  plainText = plainText.replace(imagesInEditor[j], markerWithParam);
                  //alert('Текст после замены' + markerWithParam);
              }
              for(var i = 0; i < images.length; i+=2) {
                  //alert('заменяем ' + images[i] + ' на' + images[i + 1]);
                  plainText = plainText.replace(images[i], images[i + 1]);
                  //alert('после замены ' + plainText);
              }
          }
          tinyMCE.activeEditor.setContent(plainText, {format : 'html'});
      }
  </script>
  </body>
</html>
