<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Все документы по теме "Spring"</title>
        <link rel="stylesheet" href="/css/styles.css">
    </head>
    <body>
        <h1 align="center">Все документы по теме "Spring"</h1>
        <a href="<c:url value='/create'/>">Добавить новый документ</a><br>
        <a href="<c:url value='/mindmap'/>">MindMap</a><br>
        <a href="<c:url value='/admin/users'/>">Пользователи</a><br>
        <table>
            <tr>
                <th>id</th>
                <th>Имя документа</th>
                <th style="width: 400px;">"Показать"</th>
                <th>"Редактировать"</th>
                <th>"Удалить"</th>
            </tr>
            <c:forEach var="document" items="${allDocuments}">
            <tr>
                <td>${document.documentId}</td>
                <td><b>${document.name}</b></td>
                <td><a href="<c:url value='/show/${document.documentId}'/>">Показать</a></td>
                <td><a href="<c:url value='/edit/${document.documentId}'/>">Редактировать</a></td>
                <td><a href="<c:url value='/delete/${document.documentId}'/>">Удалить</a></td>
            </tr>
            </c:forEach>
        </table>
    </body>
</html>