package slayfi.jrnotebook.util;

import java.io.*;
import java.util.Properties;

public class ResourcesUtil {

    public static String getResources(String fileName, String resName) {
        String result = null;
        Properties props = new Properties();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(new File(fileName));
            props.load(fileInputStream);
            result = props.getProperty(resName);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileInputStream != null)
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return result;
    }

    public static void setResources(String fileName, String resName, String newValue) {
        Properties props = new Properties();
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(new File(fileName));
            props.load(fileInputStream);
            fileInputStream.close();
            props.setProperty(resName, newValue);
            fileOutputStream = new FileOutputStream(new File(fileName));
            props.store(fileOutputStream, null);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
