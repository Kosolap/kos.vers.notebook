package slayfi.jrnotebook.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import slayfi.jrnotebook.model.Document;
import slayfi.jrnotebook.model.Image;
import slayfi.jrnotebook.model.Mapper;

import java.io.*;
import java.net.URL;

public class HttpClientUtil {

    public static synchronized String saveUrlImage(String pictureUrl) {
        String result = null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet("http://uploads.ru/api?upload=" + pictureUrl);

            HttpResponse response = httpClient.execute(getRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent()), "utf-8"));

            String output;
            StringBuilder builder = new StringBuilder("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                builder.append(output);
            }
            result = builder.toString();
            httpClient.getConnectionManager().shutdown();

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getUrlLoadedUrl(result);
    }

    public static synchronized String savePcImage(String path) {
        String result = null;
        HttpClient httpclient = new DefaultHttpClient();
        httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

        HttpPost httppost = new HttpPost("http://deviantsart.com/");
        File file = new File(path);

        MultipartEntity mpEntity = new MultipartEntity();
        ContentBody cbFile = new FileBody(file);
        mpEntity.addPart("userfile", cbFile);

        try {
            httppost.setEntity(mpEntity);
            result = "executing request " + httppost.getRequestLine();
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();

            StringBuilder builder = new StringBuilder(response.getStatusLine().toString());
            if (resEntity != null) {
                builder.append(EntityUtils.toString(resEntity));
            }
            result = builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        httpclient.getConnectionManager().shutdown();
        return getUrlLoadedPc(result);
    }

    /*Метод сохраняет изображение на внешнем ресурсе в зависимости от способа загрузки файла,
    присваивает image.firstUrl новую ссылку, а старую перезаписывает на image.thirdUrl.
    После этого сохраняет новые значения ссылок в БД.
    */
    public static synchronized void saveInExternalResources(final Image image) {
        new Thread(new Runnable() {
            public void run() {
                String newUrl;
                if(image.getSecondUrl().equals("-")) {
                    newUrl = saveUrlImage(image.getFirstUrl());
                    image.setThirdUrl(newUrl);
                } else {
                    newUrl = savePcImage(image.getSecondUrl());
                    image.setThirdUrl(newUrl);
                }
                Mapper.addOrUpdateImage(image);
            }
        }).start();
    }

    public static void refreshAllLinks() {
        for(Image image : Mapper.getAllImages()) {
            if(image.getThirdUrl() == null)
                saveInExternalResources(image);
        }
    }

    public static synchronized void checkLink(Image image) {
        if (image.getSecondUrl().equals("-")) {
            try {
                new URL(image.getFirstUrl()).openStream();
            } catch (FileNotFoundException fex) {
                String url = image.getThirdUrl();
                image.setThirdUrl(image.getFirstUrl());
                image.setFirstUrl(url);
                System.err.println("Изменена ссылка для " + image.getDocument().getName() + "." + image.getImageId());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            File file = new File(image.getSecondUrl());
            if (!file.exists() || !file.canRead()) {
                String url = image.getThirdUrl();
                image.setThirdUrl(image.getFirstUrl());
                image.setFirstUrl(url);
                System.err.println("Изменена ссылка для " + image.getDocument().getName() + "." + image.getImageId());
            }
        }
    }

    private static synchronized String getUrlLoadedUrl(String str) {
        String result;
        int start = str.indexOf("img_url\":\"");
        int end = str.indexOf("\",\"img_view\":");
        result = str.substring(start + 10, end);
        int first = result.indexOf("\\/\\/");
        int second = result.indexOf("uploads.ru");
        int third = result.indexOf("ru\\/");
        String part1 = result.substring(0, first);
        String part2 = result.substring(second - 3, third + 2);
        String part3 = result.substring(third + 4, result.length());

        result = part1 + "//" + part2 + "/" + part3;
        return result;
    }

    private static synchronized String getUrlLoadedPc(String str) {
        String result;
        int start = str.indexOf("\"url\":\"");
        int end = str.indexOf(".jpg");
        result = str.substring(start + 7, end + 4);
        return result;
    }

}
