package slayfi.jrnotebook.model;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "document", catalog = "test")
public class Document {
    private int documentId;
    private String name;
    private String value;
    private String mindmap;
    private Theme theme;
    private List<Comment> comments = new ArrayList<Comment>();
    private List<Image> images = new ArrayList<Image>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    public int getDocumentId() {
        return documentId;
    }

    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }

    @Column(name = "document_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "document_value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "document_mindmap")
    public String getMindmap() {
        return mindmap;
    }

    public void setMindmap(String mindmap) {
        this.mindmap = mindmap;
    }

    /*
        FetchType.EAGER означает что при чтении из БД темы, она автоматически
        вычитает все документы по этой теме и сохранит их в List<Document>.
        Второй тип FetchType.LAZY более гибкиий и предпочтительный, при нем документы
        не будут вычитываться сразу (что очень хорошо если их много), а будут читаться из базы
        отдельными подзапросами по мере обращения к ним. Но для этого нам нужно иметь постоянную открытую
        сессию hibernate. Как это грамотно сделать я пока не понял и так как у нас рабочие данные пока
        небольшие я реализовал через EAGER.
         */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "theme_id")
    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "document")
    /*Так как у нас две аннотации FetchType.EAGER в одной сущности, используем
    FetchMode.SUBSELECT
    */
    @Fetch(value = FetchMode.SUBSELECT)
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "document")
    @Fetch(value = FetchMode.SUBSELECT)
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (getDocumentId() != document.getDocumentId()) return false;
        if (getValue() != null ? !getValue().equals(document.getValue()) : document.getValue() != null) return false;
        return getTheme().equals(document.getTheme());

    }

    @Override
    public int hashCode() {
        int result = getDocumentId();
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        result = 31 * result + getTheme().hashCode();
        return result;
    }
}
