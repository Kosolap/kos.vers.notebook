package slayfi.jrnotebook.model;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import slayfi.jrnotebook.util.HibernateUtil;

import java.io.File;
import java.util.List;

public class Mapper {

    private static Session getSession() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session;
    }

    public static synchronized void addOrUpdateTheme(Theme theme) {
        Session session = getSession();
        session.getTransaction().begin();
        session.saveOrUpdate(theme);
        session.getTransaction().commit();
        session.close();
    }

    public static synchronized void deleteTheme(int themeId) {
        Theme theme = getThemeById(themeId);
        Session session = getSession();
        session.getTransaction().begin();
        session.delete(theme);
        session.getTransaction().commit();
        session.close();
    }

    @SuppressWarnings("all")
    public static synchronized List<Theme> getAllThemes() {
        Session session = getSession();
        session.getTransaction().begin();
        List<Theme> result = session.createQuery("from Theme").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized List<Theme> getAllUserThemes(int userId) {
        Session session = getSession();
        session.getTransaction().begin();
        List<Theme> result = session.createQuery("FROM Theme c WHERE c.userId = :userId").setParameter("userId",userId).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    @SuppressWarnings("all")
    public static synchronized List<Image> getAllImages() {
        Session session = getSession();
        session.getTransaction().begin();
        List<Image> result = session.createQuery("from Image").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized Theme getThemeById(int themeId) {
        Session session = getSession();
        session.getTransaction().begin();
        Theme result = (Theme) session.get(Theme.class, themeId);
        session.getTransaction().commit();
        session.close();
        return result;
    }

    @SuppressWarnings("unchecked")
    public static synchronized Theme getThemeByName(String themeName) {
        System.out.println("Olololo");
        Session session = getSession();
        session.getTransaction().begin();
        System.out.println("Start");
        Theme result = (Theme) session.createCriteria(Theme.class).add(Restrictions.eq("name", themeName)).uniqueResult();
        System.out.println("End");
        session.getTransaction().commit();
        session.close();
        return result;
    }

    @SuppressWarnings("all")
    public static synchronized List<Document> getAllDocuments() {
        Session session = getSession();
        session.getTransaction().begin();
        List<Document> result = session.createQuery("from Document").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized Document getDocumentById(int documentId) {
        Session session = getSession();
        session.getTransaction().begin();
        Document result = (Document) session.get(Document.class, documentId);
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized Document getDocumentByName(String documentName) {
        Session session = getSession();
        session.getTransaction().begin();
        Document result = (Document) session.createCriteria(Document.class).add(Restrictions.eq("name", documentName)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized void deleteDocument(int documentId) {
        Document document = getDocumentById(documentId);
        Session session = getSession();
        session.getTransaction().begin();
        session.delete(document);
        session.getTransaction().commit();
        session.close();
    }

    public static synchronized void addOrUpdateDocument(Document document) {
        Session session = getSession();
        session.getTransaction().begin();
        session.saveOrUpdate(document);
        session.getTransaction().commit();
        session.close();
    }

    public static synchronized void addOrUpdateImage(Image image) {
        Session session = getSession();
        session.getTransaction().begin();

        Image forCheck = getImageByName(image.getName());
        if(forCheck != null) image.setImageId(forCheck.getImageId());

        session.saveOrUpdate(image);
        session.getTransaction().commit();
        session.close();
    }

    public static synchronized Image getImageById(int imageId) {
        Session session = getSession();
        session.getTransaction().begin();
        Image result = (Image) session.get(Image.class, imageId);
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized Image getImageByName(String imageName) {
        Session session = getSession();
        session.getTransaction().begin();
        Image result = (Image) session.createCriteria(Image.class).add(Restrictions.eq("name", imageName)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized String getImageBsCode(String imageName) {
        Image resImage = getImageByName(imageName);
        String result = resImage.getBScode();
        return result;
    }

    public static synchronized void deleteImage(int imageId) {
        Image image = getImageById(imageId);
        Session session = getSession();
        session.getTransaction().begin();
        session.delete(image);
        session.getTransaction().commit();
        session.close();
        if(!image.getSecondUrl().equals("-"))
            deleteImageFromServer(image);
    }

    private static synchronized void deleteImageFromServer(Image image) {
        File file = new File(image.getSecondUrl());
        System.out.println((file.delete()));
    }
}
