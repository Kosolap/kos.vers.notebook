package slayfi.jrnotebook.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "theme", catalog = "test")
public class Theme {
    private int id;
    private String name;
    private int userId;
    private List<Document> documents = new ArrayList<Document>();

    public Theme() {}

    public Theme(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "theme_id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "theme_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "user_id")
    public int getUserId(){
        return userId;
    }

    public void setUserId(int userId){
        this.userId = userId;
    }

    //См. объяснения в Document
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "theme")
    /*При сохранении/обновлении/удалении темы, связанные с ним документы
    hibernate изменяет автоматически. К примеру если мы изменили в нешей теме в
    List<Document> documents один из документов и вызвали Mapper.addOrUpdateTheme()
    соответсвующая запись документа в БД изменится автоматически.
    */
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Theme theme = (Theme) o;

        if (getId() != theme.getId()) return false;
        if (!getName().equals(theme.getName())) return false;
        return !(getDocuments() != null ? !getDocuments().equals(theme.getDocuments()) : theme.getDocuments() != null);

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getDocuments() != null ? getDocuments().hashCode() : 0);
        return result;
    }
}
