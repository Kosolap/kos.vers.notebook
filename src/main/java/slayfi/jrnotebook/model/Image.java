package slayfi.jrnotebook.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "image", catalog = "test")
public class Image {
    private int imageId;
    private String name;
    private String firstUrl;
    private String secondUrl;
    private String thirdUrl;
    private Document document;
    private String bsCode;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    @Column(name = "image_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "image_first_url")
    public String getFirstUrl() {
        return firstUrl;
    }

    public void setFirstUrl(String firstUrl) {
        this.firstUrl = firstUrl;
    }

    @Column(name = "image_second_url")
    public String getSecondUrl() {
        return secondUrl;
    }

    public void setSecondUrl(String secondUrl) {
        this.secondUrl = secondUrl;
    }

    @Column(name = "image_third_url")
    public String getThirdUrl() {
        return thirdUrl;
    }

    @Column(name = "bs_code")
    public String getBScode(){
        return bsCode;
    }

    public void setBScode(String bsCode) {
        this.bsCode = bsCode;
    }

    public void setThirdUrl(String thirdUrl) {
        this.thirdUrl = thirdUrl;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id")
    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
