package slayfi.jrnotebook.model;

/**
 * Created by Kosolap on 21.05.2015.
 */
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Created by Kosolap on 18.05.2015.
 */

@Entity
@Table(name = "user_roles" , catalog = "test")
public class UserRole {
    private int userRoleId;
    private String userRole;
    private String userName;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_role_id")
    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    @Column(name = "role")
    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Column(name = "username")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}