package slayfi.jrnotebook.service.login;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import slayfi.jrnotebook.model.UserRole;
import slayfi.jrnotebook.service.UserService;

@Service("loginService")
public class LoginServiceImpl implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        slayfi.jrnotebook.model.User user = UserService.getUserByLogin(username);

        List<GrantedAuthority> authorities = buildUserAuthority(UserService.getUserRoleByLogin(user.getUserLogin()));

        return buildUserForAuthentication(user, authorities);
    }

    private User buildUserForAuthentication(slayfi.jrnotebook.model.User user,
                                            List<GrantedAuthority> authorities) {
        return new User(user.getUserLogin(), user.getUserPassword(),
                user.isEnabled(), true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        // Build user's authorities
        for (UserRole userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getUserRole()));
        }

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(
                setAuths);

        return Result;
    }

}