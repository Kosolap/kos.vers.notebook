package slayfi.jrnotebook.service;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import slayfi.jrnotebook.model.User;
import slayfi.jrnotebook.model.UserRole;
import slayfi.jrnotebook.util.HibernateUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kosolap on 18.05.2015.
 */
public class UserService {

    private static Session getSession(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session;
    }

    public static synchronized void addOrUpdateUser(User user){
        Session session = getSession();
        session.getTransaction().begin();
        session.saveOrUpdate(user);
        session.getTransaction().commit();
        session.close();
    }

    public static synchronized User getUserByLogin(String login){
        Session session = getSession();
        session.getTransaction().begin();
        User result = (User) session.createCriteria(User.class).add(Restrictions.eq("userLogin", login)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized User getUserById(int userId){
        Session session = getSession();
        session.getTransaction().begin();
        User result = (User) session.get(User.class, userId);
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public static synchronized Set<UserRole> getUserRoleByLogin(String login){
        Session session = getSession();
        session.getTransaction().begin();
        List<UserRole> result = session.createQuery("FROM UserRole c WHERE c.userName = :login").setParameter("login",login).list();
        Set<UserRole> result2 = new HashSet<UserRole>(result);
        session.getTransaction().commit();
        session.close();
        return result2;
    }

    public static synchronized void deleteUserById(int userId){
        User user = getUserById(userId);

        Session session = getSession();
        session.getTransaction().begin();
        session.delete(user);
        session.getTransaction().commit();
        session.close();
    }

    //@SuppressWarnings("all")
    public static synchronized List<User> getAllUsers(){

        Session session = getSession();
        session.getTransaction().begin();

        List<User> users = (List<User>) session.createQuery("from User").list();

        session.getTransaction().commit();
        session.close();

        return users;
    }
}
